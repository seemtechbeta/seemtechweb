import HomePage from "./components/page/home";

export default class Url {
    static get HomePage() {
        return '/';
    }
    static get LoginPage() {
        return '/login';
    }
    static get Dashboard() {
        return '/dashboard';
    }
}
