import React from 'react'
import {Route,Redirect, Switch} from 'react-router-dom'
import LoginPage from './components/page/log_in'
import HomePage from './components/page/home'
import Username from './components/pages/username'
import Dashboard from './components/pages/dashboard'
import Url from './url'

const App = () => {
    let routeArray=[
        <PrivateRoute exact path='/username' component={Username} />,
        <Route exact path={Url.LoginPage} component={LoginPage} />,
        <Route exact path={Url.HomePage} component={HomePage} />
    ];
    routeArray.push(<PrivateRoute exact path={Url.Dashboard} component={Dashboard} />);
    return (
        <div>
            <Switch>
                {routeArray}
            </Switch>
        </div>
    )
};

const PrivateRoute = ({component: Component, ...rest}) => {
    return (
        <Route
            {...rest}
            render={props => (
                (!window.SEEM_LOGGED_IN_USER || window.SEEM_LOGGED_IN_USER === null) ? (
                    <Redirect to={{
                        pathname: Url.LoginPage,
                        state: {from: props.location}
                    }}
                    />
                ) : (
                    <div className="container-fluid">
                        <div className="row">

                            <div className="col-md-10 margin-left-17 fill-container-left-right">
                                <div className="margin-top-7">
                                    <Component {...props} /></div>
                            </div>
                        </div>
                    </div>
                )
            )}
        />
    )
};


export default App;
