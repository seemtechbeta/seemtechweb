import React from 'react'
import AppComponent from "../AppComponent";
import FacebookProvider, {Login} from 'react-facebook';
import GoogleLogin from 'react-google-login';

export default class SocialLoginPage extends AppComponent {
    constructor(props) {
        super(props);
    }

    render() {
        return (<div>
                <div className="element-spacing">
                    <GoogleLogin
                        clientId="792915091117-6j1ioph846bik0o76jgrgnc709fvehd8.apps.googleusercontent.com"
                        onSuccess={this.props.responseGoogle}
                        className={"rcorner col-md-10 offset-1"}
                    >
                        <h4 className="h4-font-color inline-display">Continue with Google</h4>
                        <span className="fa fa-google pull-right"/>
                    </GoogleLogin>
                </div>
                <FacebookProvider appId="251949192289597">
                    <Login
                        onResponse={this.props.handleResponseFacebook}
                        onFaluire={this.props.handleError}
                    >
                        <button className="rcorner col-md-10 offset-1 margin-top-20">
                            <h4 className="h4-font-color inline-display">Continue with Facebook</h4>
                            <span className="fa fa-facebook pull-right"/>
                        </button>
                    </Login>
                </FacebookProvider>
            </div>
        )
    }
}
