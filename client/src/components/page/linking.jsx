import React from 'react'
import AppComponent from "../AppComponent";

export default class LinkingPage extends AppComponent{

    render() {
        return (
            <div className="container-fluid padding-left-0 padding-right-0">
                <div className="bg">
                    <div className="linking-wrapper">
                        <div className="box-wrapper">
                            <div className="prgress-bar-wrp">
                                <div className="prgress-bar-container">                                    
                                    <ul className="prgbar">
                                        <li className="active"></li>
                                        <li className="active"></li>
                                        <li></li>
                                    </ul>
                                </div>
                                <div className="clearfix"></div>
                                <h2>Get credits for your work, Costomize your Collar</h2>
                            </div>

                            <div className="linking-container">
                                <div className="profile-panel">
                                    <div className="user-profile">
                                        <div className="upload-profile-pic"></div>
                                        <div className="user-info">
                                            <div className="user-name">Shivam Varshney</div>
                                            <div className="account-linked"></div>
                                        </div>
                                    </div>
                                    <p className="profile-text">This is how your collar will be shown</p>
                                </div>
                                <div className="account-panel">
                                    <div>
                                        <div className="blank-pic"></div>
                                        <p>Upload Profile Picture</p>
                                    </div>  
                                    <div>
                                        <div>
                                            <div className="logo"></div>
                                            <div className="logo-text">Linked Facebook account</div>
                                        </div>
                                        <div>
                                            <div className="logo"></div>
                                            <div className="logo-text">Link your Twitter account</div>
                                        </div>
                                        <div>
                                            <div className="logo"></div>
                                            <div className="logo-text">Link your Instagram account</div>
                                        </div>
                                        <div>
                                            <div className="logo"></div>
                                            <div className="logo-text">Link your Gmail account</div>
                                        </div>
                                        <div>
                                            <div className="logo"></div>
                                            <div className="logo-text">Linked Facebook account</div>
                                        </div>                                        
                                        <div>
                                            <div className="logo"></div>
                                            <input type="text" className="rcorner-text-field col-md-4 offset-1" placeholder="" />
                                        </div>
                                        <div>
                                            <div className="logo"></div>
                                            <input type="text" className="rcorner-text-field col-md-4 offset-1" placeholder="" />
                                        </div>
                                    </div>                                  
                                </div>
                            </div>
                            <div className="user-action-wrp">                            
								<form id="link_form" name="link_form" action="" method="POST">
									<a href="#" onClick="submitForm()" title="Submit" className="button-submit">Submit</a>									
								</form>									
								<a href="javascript:void(0)" id="cancel" title="skip" className="button-skip">Skip this step </a>								
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
