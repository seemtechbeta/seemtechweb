import React from 'react'
import AppComponent from "../AppComponent";
import $ from "jquery";
import _ from 'underscore';
import Url from '../../url';
import SocialLogin from "../common/social_login";
import moment from 'moment';

import 'react-datepicker/dist/react-datepicker.css';


export default class HomePage extends AppComponent {
    constructor(props) {
        super(props);
        _.bindAll(this, 'signUp', 'navigateTo', 'handleResponseFacebook', 'responseGoogle', 'handleError');
    }

    componentDidMount() {
        if(window.SEEM_LOGGED_IN_USER != 'user' && window.SEEM_LOGGED_IN_USER){
            this.navigateTo('/dashboard')
        }
    }

    responseGoogle(data) {
        const email = data["profileObj"]["email"];
        const password  = data["googleId"];
        const authToken =  data["tokenId"];
        const authProvider  = 'google';
        window.SEEM_LOGGED_IN_USER = 'user';
        window.localStorage.setItem('token', authToken);
        const state = {
            email: email,
            password: password,
            authProvider: authProvider,
        };
        this.navigateTo('/username', state);

    }

    handleResponseFacebook(data) {
        const email = data["profile"]["email"];
        const password = data["profile"]["id"];
        const token = data["tokenDetail"]["accessToken"];
        const authProvider = 'facebook';
        window.SEEM_LOGGED_IN_USER = 'user';
        window.localStorage.setItem('token', token);
        const state = {
            email: email,
            password: password,
            authProvider: authProvider
        };
        this.navigateTo('/username', state);
    };

    handleError(data) {
        console.log(data)
    }

    signUp(e) {
        e.preventDefault();
        const name = $('#name').val();
        const email = $('#email').val();
        const username = $('#username').val();
        const password = $('#password').val();
        const day = $('#day').val();
        const month = $('#month').val();
        const year = $('#year').val();
        debugger;
        const date= moment([year, month, day]).format('YYYY-MM-DD');
        $.ajax({
            url: '/api/users/create',
            type: 'POST',
            data: {"user": {email: email, username: username, password: password}},
            success: (response) => {
                window.SEEM_LOGGED_IN_USER = response.msg;
                window.localStorage.setItem('token', response.msg.auth_token);
                this.navigateTo(Url.Dashboard)
            },
            error: (errorResponse) => {
                window.SEEM_LOGGED_IN_USER = "";
                window.clearStorage();
            }
        });
    }

    render() {
        return <div className="container-fluid padding-left-0 padding-right-0">
            <div className="bg">
                <div className="col-md-4 offset-4 top-spacing bottom-spacing">
                <div className="box-sizing">
                    <h3 className="col-md-6 offset-3 element-spacing grey-font-color">Join the community of
                        kickass photographers</h3>
                    <SocialLogin
                        handleResponseFacebook={this.handleResponseFacebook}
                        handleError={this.handleError}
                        responseGoogle={this.responseGoogle}
                    />
                    <div className="offset-1">
                    <h5 className="line-with-text element-spacing"><span>OR</span></h5>
                    </div>
                    <h3 className="text-center grey-font-color">Sign up</h3>
                    <h4 className="grey-font-color col-md-10 offset-1 padding-left-0 align-left margin-bottom-0">Name*</h4>
                    <input type="text" className="rcorner-text-field col-md-10 offset-1" placeholder="" id='name'/>
                    <h4 className="grey-font-color col-md-10 offset-1 padding-left-0 align-left margin-bottom-0">User Name*</h4>
                    <input type="text" className="rcorner-text-field col-md-10 offset-1" placeholder="" id='username'/>
                    <h4 className="grey-font-color col-md-10 offset-1 padding-left-0 align-left margin-bottom-0">Email ID*</h4>
                    <input type="text" className="rcorner-text-field col-md-10 offset-1" placeholder="" id='email'/>
                    <h4 className="grey-font-color col-md-10 offset-1 padding-left-0 align-left margin-bottom-0">Password*</h4>
                    <input type="password" className="rcorner-text-field col-md-10 offset-1" placeholder="" id='password'/>
                    <h5 className="col-md-10 offset-1 align-left padding-left-0">Min. 6 characters with 1 numeric and 1 alphabet</h5>
                    <h4 className="grey-font-color col-md-10 offset-1 padding-left-0 align-left margin-bottom-0">Date of Birth*</h4>
                    <select className="offset-1 col-md-4 rcorner-text-field padding-top-3 padding-bottom-3 styled-select" id='month'>
                    <option value='1'>Janaury</option>
                    <option value='2'>February</option>
                    <option value='3'>March</option>
                    <option value='4'>April</option>
                    <option value='5'>May</option>
                    <option value='6'>June</option>
                    <option value='7'>July</option>
                    <option value='8'>August</option>
                    <option value='9'>September</option>
                    <option value='10'>October</option>
                    <option value='11'>November</option>
                    <option value='12'>December</option>
                    </select>
                    <select className="margin-left-18 col-md-2 rcorner-text-field padding-top-3 padding-bottom-3 styled-select" id='day'>
                        {(() => {
                            const days = [];
                            for (let index = 1; index <= 31; index +=1) {
                                days.push(<option>{index}</option>);
                            }
                            return days;
                        })()}
                    </select>
                    <select className="margin-left-18 col-md-3 rcorner-text-field padding-top-3 padding-bottom-3 styled-select" id='year'>
                    {(() => {
                        const years = [];
                     for (let index = 1965; index <= 2018; index +=1) {
                      years.push(<option>{index}</option>);
                     }
                     return years;
                    })()}
                      </select>
                    <h4 className="grey-font-color col-md-10 offset-1 padding-left-0 align-left padding-top-5">Gender*</h4>
                    <select className="col-md-10 offset-1 padding-left-0 rcorner-text-field styled-select" id='gender'>
                        <option>
                        </option>
                        <option>&nbsp; Male </option>
                        <option>&nbsp; Female </option>
                        <option>&nbsp; Others </option>
                    </select>
                    <h4 className="grey-font-color col-md-10 offset-1 padding-left-0 align-left">By clicking continue you agree to
                        <a href="#"> terms and conditions</a>
                    </h4>
                    <button className="submit-btn-bg col-md-4 offset-4"><h4 className="font-white margin-bottom-0" onClick={this.signUp}>Submit</h4></button>
                    <h4 className="grey-font-color col-md-10 offset-1 padding-left-0 align-left margin-top-20">Already a member?  <a href="#">Log In</a></h4>
                </div>
                </div>
        </div>
        </div>
    }
}
