import React from 'react'
import AppComponent from "../AppComponent";
import $ from "jquery";
import _ from 'underscore';
import SocialLogin from "../common/social_login";

export default class LoginPage extends AppComponent {
    constructor(props) {
        super(props);

        _.bindAll(this, 'login', 'navigateTo');
    }
    componentDidMount() {
        if(window.SEEM_LOGGED_IN_USER != 'user' && window.SEEM_LOGGED_IN_USER){
            this.navigateTo('/dashboard')
        }
    }
    login(e) {
        var email = $('#email').value();
        var username = $('#username').value();
        var password = $('#password').value();
        e.preventDefault();
        $.ajax({
            url: '/api/users/user_token',
            type: 'POST',
            data: {"auth": {email: email, username: username, password: password}},
            success: (response) => {
                window.SEEM_LOGGED_IN_USER = response.user;
                window.localStorage.setItem('token', response.token);
                this.navigateTo('/dashboard')
            },
            error: () => {
                window.SEEM_LOGGED_IN_USER = "";
                window.clearStorage();
            }
        });
    }

    render() {
        return <div className="container-fluid padding-left-0 padding-right-0">
            <div className="bg">
                <div className="col-md-4 offset-4 top-spacing bottom-spacing">
                    <div className="box-sizing-2">
                        <h3 className="col-md-6 offset-3 element-spacing grey-font-color">Lets get on board,
                            And kick some ass!</h3>
                        <SocialLogin/>
                        <div className="offset-1">
                            <h5 className="line-with-text element-spacing font-black"><span>OR</span></h5>
                        </div>
                        <h3 className="text-center grey-font-color font-black">Login Using email</h3>
                        <h4 className="grey-font-color col-md-10 offset-1
                                padding-left-0 align-left margin-bottom-0 font-black text-box-spacing-top">
                            Email ID
                        </h4>
                        <input type="text" className="rcorner-text-field col-md-10 offset-1" placeholder="" id="email"/>
                        <h4 className="grey-font-color col-md-10 offset-1 padding-left-0 align-left margin-bottom-0 font-black text-box-spacing-top">Username</h4>
                        <input type="text" className="rcorner-text-field col-md-10 offset-1" placeholder=""
                               id='username'/>
                        <div className="col-md-11 text-box-spacing-top offset-1 padding-left-0">
                            <h4 className="grey-font-color align-left inline-display font-black">Password</h4>
                            <h4 className="grey-font-color margin-bottom-0 pull-right offset-1 padding-right-20">Forgot
                                Password?</h4>
                        </div>
                        <input type="password" className="rcorner-text-field col-md-10 offset-1" placeholder=""
                               id="password"/>
                        <button className="submit-btn-bg col-md-4 offset-4 margin-top-30" onClick={this.login}>
                            <h4 className="font-white margin-bottom-0">Submit</h4></button>
                        <h4 className="grey-font-color col-md-10 offset-1 padding-left-0 center margin-top-20">Not a
                            member? <a href="#">Sign Up</a></h4>
                    </div>
                </div>
            </div>
        </div>
    }
}
