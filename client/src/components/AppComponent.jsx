import React from 'react';
import {createBrowserHistory} from 'history';

export default class AppComponent extends React.Component {
    constructor(props) {
        super(props);
        this.navigateTo = this.navigateTo.bind(this)
    }

    navigateTo(url, state = {}) {
        this.props.history.push(url, state);
    }
}
