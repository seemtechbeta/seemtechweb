import React from 'react';
import $ from 'jquery';
import AppComponent from '../AppComponent';
import FacebookProvider, {Login} from 'react-facebook';


export default class Welcome extends AppComponent {
    constructor(props) {
        super(props);
        this.signUp = this.signUp.bind(this);
        this.login = this.login.bind(this);
        this.handleResponse = this.handleResponse.bind(this);
        this.responseGoogle = this.responseGoogle.bind(this);
        this.handleError = this.handleError.bind(this);
    }

    componentDidMount() {
        if (window.SEEM_LOGGED_IN_USER != 'user' && window.SEEM_LOGGED_IN_USER)
            this.navigateTo('/dashboard')
    }

    signUp(e) {
        e.preventDefault();
        $.ajax({
            url: '/api/users/create',
            type: 'POST',
            data: {"user": {email: "prabh12@gmail.com", username: "sdaassd", password: "Password12"}},
            success: (response) => {
                window.SEEM_LOGGED_IN_USER = response.msg;
                window.localStorage.setItem('token', response.msg.auth_token);
                this.navigateTo('/dashboard')
            },
            error: (errorResponse) => {
                window.SEEM_LOGGED_IN_USER = "";
                window.clearStorage();
                // this.setState({errorMessage: "Please Enter a Valid Username and Password"})
            }
        });
    }

    login(e) {
        e.preventDefault();
        $.ajax({
            url: '/api/users/user_token',
            type: 'POST',
            data: {"auth": {email: "prabh12@gmail.com", username: "sdaassd", password: "Password12"}},
            success: (response) => {
                window.SEEM_LOGGED_IN_USER = response.user;
                window.localStorage.setItem('token', response.token);
                this.navigateTo('/dashboard')
            },
            error: () => {
                window.SEEM_LOGGED_IN_USER = "";
                window.clearStorage();
                // this.setState({errorMessage: "Please Enter a Valid Username and Password"})
            }
        });
    }

    handleResponse(data){
        const email = data["profile"]["email"];
        const password  = data["profile"]["id"];
        const token = data["profile"][""];
        const authProvider  = 'facebook';
        window.SEEM_LOGGED_IN_USER =  'user';
        window.localStorage.setItem('token', password);
        const state = {
            email: email,
            password: password,
            authProvider: authProvider
        };
        this.navigateTo('/username', state);
    };

    responseGoogle(data) {
        const email = data["profileObj"]["email"];
        const password  = data["googleId"];
        const authToken =  data["tokenId"]
        const authProvider  = 'google';
        window.SEEM_LOGGED_IN_USER =  'user';
        window.localStorage.setItem('token', authToken);
        const state = {
            email: email,
            password: password,
            authProvider: authProvider,
        };
        this.navigateTo('/username', state);

    }

    handleError = (error) => {
     console.log(error)
    }

    render() {
        return (
            <div>
                <FacebookProvider appId="251949192289597">
                    <Login
                        onResponse={this.handleResponse}
                        onError={this.handleError}
                    >
                        <button>
                            Login via Facebook
                        </button>
                    </Login>
                </FacebookProvider>

                <button onClick={this.signUp}>SignUp</button>
                <button onClick={this.login}>Login</button>
            </div>
        )
    }
}
