import React from 'react';
import AppComponent from '../AppComponent';

export default class Dashboard extends AppComponent{
    constructor(props){
        super(props)
        this.logout = this.logout.bind(this);
    }
    logout(event) {
        event.preventDefault();
        window.clearStorage();
        window.SEEM_LOGGED_IN_USER = null;
        this.props.history.push('/login');
    }
    render(){
        return(
            <div>
                Welcome User
                <button onClick={this.logout}>LogOut</button>
            </div>
        )
    }
}