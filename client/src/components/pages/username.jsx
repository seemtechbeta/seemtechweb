import React from 'react';
import AppComponent from '../AppComponent';
import $ from 'jquery';

export default class Username extends AppComponent {
    constructor(props) {
        super(props);
        this.createUser = this.createUser.bind(this);
    }

    createUser(e) {
        e.preventDefault();
        const userDetails = this.props.location.state;
        const userName = $('#username').val();
        $.ajax({
            url: '/api/users/create',
            type: 'POST',
            data: {
                "user":
                    {
                        email: userDetails.email,
                        username: userName,
                        password: userDetails.password,
                        auth_provider: userDetails.authProvider
                    }
            },
            success: (response) => {
                window.SEEM_LOGGED_IN_USER = response.msg;
                window.localStorage.setItem('token', response.msg.auth_token);
                this.navigateTo('/dashboard')
            },
            error: (errorResponse) => {
                window.SEEM_LOGGED_IN_USER = "";
                window.clearStorage();
                // this.setState({errorMessage: "Please Enter a Valid Username and Password"})
            }
        });
    }

    render() {
        return (
            <div>
                <input id='username' type='text'/>
                <button onClick={this.createUser}> Submit</button>
            </div>
        )
    }
}