import React from 'react'
import ReactDOM from 'react-dom'
import Routes from './routes'
import {BrowserRouter} from 'react-router-dom'
import Popper from 'popper.js';
import './index.scss'
import $ from 'jquery'

$(document).ready(function () {
    window.SEEM_APP_STORAGE = window.localStorage
    window.userInit = function (user) {
        window.SEEM_LOGGED_IN_USER = user
    }

    window.storeToken = function (token) {
        window.SEEM_APP_STORAGE.setItem('token', token)
    }

    window.clearStorage = function () {
        window.SEEM_APP_STORAGE.clear()
    }

    window.getMe = function () {
        $.ajax({
            url: '/api/users/current',
            type: 'GET',
            headers: {'Authorization': window.getJwtToken()},
            success: function (response) {
                window.SEEM_LOGGED_IN_USER = response;
                if(!window.SEEM_LOGGED_IN_USER){
                    window.clearStorage()
                }
                ReactDOM.render(
                    <BrowserRouter>
                        <Routes/>
                    </BrowserRouter>,
                    document.getElementById('root')
                )
            },
            error: (response) => {
                ReactDOM.render(
                    <BrowserRouter>
                        <Routes/>
                    </BrowserRouter>,
                    document.getElementById('root')
                )
            }
        })
    }
    window.getJwtToken = function () {
        return `Bearer ${window.SEEM_APP_STORAGE.getItem('token')}`
    }
    window.getMe()
})
