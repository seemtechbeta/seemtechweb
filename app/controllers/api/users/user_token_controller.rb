class Api::Users::UserTokenController < Knock::AuthTokenController
  skip_before_action :verify_authenticity_token, raise: false
  def create
    user = User.find_by(username: params[:auth][:username])
    user.update(auth_token: auth_token.token)
    render json: {
        user: User.find_by(username: params[:auth][:username]),
        token: auth_token.token,
        status: 200
    }, status: 200
  end

  # Setting up strict parameters for when we add account creation.
  def user_params
    params.require(:user).permit(:username, :email)
  end

end
