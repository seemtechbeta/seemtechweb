class Api::Users::UsersController < Api::BaseController

  # Use Knock to make sure the current_user is authenticated before completing request.
  before_action :authenticate_user, only: [:index, :current, :update]
  before_action :authorize, only: [:update]

  # Should work if the current_user is authenticated.
  def index
    render json: {status: 200, msg: 'Logged-in'}
  end

  # Call this method to check if the user is logged-in.
  # If the user is logged-in we will return the user's information.
  def current
    render json: current_user
  end

  def create
    check_user = User.find_by(username: params[:user][:username]) || User.find_by(email: params[:user][:email])

    if check_user.present?
      check_auth_provider = check_user.authentication_providers.find_by(provider: params[:user][:auth_provider])
      if check_auth_provider.present?
        render json: {user: check_user, msg: "User already exists"}, status: 200
        return
      end
      AuthenticationProvider.create!(
          user_id: check_user.id,
          provider: params[:user][:auth_provider],
          uid: params[:user][:password]
      )
      render json: {user: nil, msg: "User successfully linked with #{params[:user][:auth_provider]}"}, status: 200
      return
    end
    new_user = User.new(user_params)
    if new_user.save
      auth_token = Knock::AuthToken.new(payload: {sub: new_user.id}).token
      new_user.update(auth_token: auth_token)
      AuthenticationProvider.create!(user_id: new_user.id, provider: params[:user][:auth_provider], uid: params[:user][:password])
      render json: {status: 200, msg: new_user}, status: 200
      return
    end
    render json: {status: 400, msg: "Cannot save the user"}, status: 400
  end

  # Method to update a specific user. User will need to be authorized.
  def update
    user = User.find(params[:id])
    if user.update(user_params)
      render json: {status: 200, msg: 'User details have been updated.'}
    end
  end

  private

  # Setting up strict parameters for when we add account creation.
  def user_params
    params.require(:user).permit(
        :username, :email, :password, :password_confirmation)
  end

  # Adding a method to check if current_user can update itself.
  # This uses our UserModel method.
  def authorize
    return_unauthorized unless current_user && current_user.can_modify_user?(params[:id])
  end
end
