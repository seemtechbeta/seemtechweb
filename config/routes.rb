Rails.application.routes.draw do

  namespace :api do
    namespace :users do
      post 'user_token' => 'user_token#create'

      # User actions
      get 'current' => 'users#current'
      post 'create' => 'users#create'
      patch ':id' => 'users#update'
      delete ':id' => 'users#destroy'
    end
  end
end
