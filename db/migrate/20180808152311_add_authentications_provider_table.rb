class AddAuthenticationsProviderTable < ActiveRecord::Migration[5.2]
  def up
    create_table :authentication_providers do |t|
      t.references :user
      t.string :provider
      t.string :uid

      t.timestamps null: false
    end

    add_index :authentication_providers, :provider
    add_index :authentication_providers, :uid
  end

  def down
    drop_table :authentication_providers
  end

end
